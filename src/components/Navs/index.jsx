import React from "react";
import {Link} from "react-router-dom";
import logo from "../../assets/img/logoPeace.png";

const links = [
    {id: 1, to: "home", title: "Home"},
    {id: 2, to: "services", title: "Services"},
    {id: 3, to: "contact", title: "Contact"},
];

const Navbar = () => {
    return (
        <nav className="header-nav">
            <nav className="navbar navbar-expand-lg header-nav">
                <Link
                    to="/"
                    // spy={true}
                    // smooth={true}
                    // offset={-70}
                    // duration={500}
                    //   activeClass="active"
                    className="navbar-brand position-relative"
                >
                    <img src={logo} alt="logo" width="50" height="auto"/>
                    <span className="logo-main-name">Mesteqs </span>
                    <span className="logo-description">Textile company</span>
                </Link>

                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav m-auto">
                        {links.map(({id, to, title}) => (
                            <li key={id} className="nav-item">
                                <Link
                                    key={id}
                                    to={to}
                                    // spy={true}
                                    // smooth={true}
                                    // offset={-70}
                                    // duration={500}
                                    // activeClass="active"
                                    className="nav-link"
                                >
                                    {title}
                                </Link>
                            </li>
                        ))}
                    </ul>
                    <Link
                        to="fashion"
                        // spy={true}
                        // smooth={true}
                        // offset={-70}
                        // duration={500}
                        // activeClass="active"
                    >
            <span className="right-front">
              Unique &#38; Extraordinary Quality!
            </span>
                    </Link>
                </div>
            </nav>
        </nav>
    );
};

export default Navbar;
