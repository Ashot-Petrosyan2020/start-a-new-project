import React from "react";

import ProductTitle from "../../containers/Data/ProductItems/Title";
import ProductLawEnforceJackets from "../../containers/Data/ProductItems/LawEnforceJackets";
import ProductPythonJackets from "../../containers/Data/ProductItems/PythonJackets";
import ProductSuedeJackets from "../../containers/Data/ProductItems/SuedeJackets";

export const productItems = [
    <ProductTitle/>,
    <ProductLawEnforceJackets/>,
    <ProductPythonJackets/>,
    <ProductSuedeJackets/>
];