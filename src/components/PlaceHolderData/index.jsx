export const data = [
    {id: 1, title: `Title`, code: `0240`},
    {
        id: 4,
        title: `Leather jackets for law enforcement agencies`,
        code: `123456`,
    },
    {id: 6, title: `Leather jackets with python skin`, code: `012345`},
    {id: 38, title: `Suede jackets`, code: `775599`},
];