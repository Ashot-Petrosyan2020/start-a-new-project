export const baseURL = `https://admin.mesteqs.com/`;

export const productsUrl = `${baseURL}api/Products/GetAll?PageSize=4&PageNumber=1`;
// export const categoriesUrl = `${baseURL}api/Categories/GetAll?MaxResultCount=2000&SkipCount=0`;
export const productCardUrl = `${baseURL}api/products/get?id=1`;
