import React from "react";
import staff from "../../assets/img/staff.png";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import FormatQuoteIcon from "@material-ui/icons/FormatQuote";

const Connection = () => {
    return (
        <Card className="mesteqs-card-container">
            <CardActionArea className="mesteqs-card-area">
                <CardMedia title="Mesteqs">
                    <img src={staff} alt="product" width="100%"/>
                </CardMedia>
                <CardContent>
                    <FormatQuoteIcon className="mesteqs-quote-icon"/>
                    <Typography variant="h6">ПОЛНОЕ СОЕДИНЕНИЕ.</Typography>
                    <div>
                        Надежный специалист, надежный контакт, дружелюбный голос - наши
                        клиенты знают нас как людей, а не как юридическое лицо.
                    </div>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default Connection;
