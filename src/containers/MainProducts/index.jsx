import React from "react";
import product from "../../assets/img/product.png";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import FormatQuoteIcon from "@material-ui/icons/FormatQuote";

const productDetails = [
    {id: 1, title: "свитера для силовых структур, кожаные куртки для силовых структур"},
    {id: 2, title: " кожаные куртки, плащи, пиджаки, жилеты, замшевые куртки"},
    {
        id: 3,
        title: "кожаные куртки с добавками кожи питона, крокодила, страуса",
    },
];

const MainProducts = () => {
    return (
        <Card className="mesteqs-card-root">
            <CardActionArea className="mesteqs-card-area">
                <CardMedia title="Mesteqs">
                    <img src={product} alt="product" width="100%"/>
                </CardMedia>
                <CardContent>
                    <FormatQuoteIcon className="mesteqs-quote-icon"/>
                    <Typography variant="h6">Изделие компании MESTEQS</Typography>
                    <div>
                        <ul>
                            {productDetails.map(({id, title}) => (
                                <li key={id}>{title}</li>
                            ))}
                        </ul>
                    </div>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default MainProducts;
