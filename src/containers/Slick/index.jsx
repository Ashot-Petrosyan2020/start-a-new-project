import React from "react";
import Slider from "react-slick";
import Connection from "../Connection";
import Products from "../MainProducts";
import Employee from "../Employee";

const Slick = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    return (
        <Slider {...settings}>
            <div className="slick-slider-item">
                <Connection/>
            </div>
            <div className="slick-slider-item">
                <Products/>
            </div>
            <div className="slick-slider-item">
                <Employee/>
            </div>
        </Slider>
    );
};

export default Slick;
