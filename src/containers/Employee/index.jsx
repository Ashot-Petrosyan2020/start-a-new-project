import React from "react";
import employee from "../../assets/img/employees.png";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import FormatQuoteIcon from "@material-ui/icons/FormatQuote";

const Employee = () => {
    return (
        <Card className="mesteqs-card-container">
            <CardActionArea className="mesteqs-card-area">
                <CardMedia title="Mesteqs">
                    <img src={employee} alt="product" width="100%"/>
                </CardMedia>
                <CardContent>
                    <FormatQuoteIcon className="mesteqs-quote-icon"/>
                    <Typography variant="h6">РАБОТА @ MESTEQS</Typography>
                    <div>
                        Мотивированные, квалифицированные и вдохновленные сотрудники
                        необходимы для постоянного успеха Mesteqs. Именно поэтому мы
                        стремимся поддерживать и развивать всех,
                        кто работает в Mesteqs, чтобы противостоять не только сегодняшним
                        вызовам, но и тем, которые стоят перед нами.
                    </div>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default Employee;
