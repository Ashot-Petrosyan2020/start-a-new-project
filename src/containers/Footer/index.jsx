import React from "react";
import Grid from "@material-ui/core/Grid";
import CallIcon from "@material-ui/icons/Call";
import EmailIcon from "@material-ui/icons/Email";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import logo from "../../assets/img/logoPeace.png";
import threadNeedle from "../../assets/img/threadNeedle1.png";
import {Link} from "react-scroll";
import {mesteqsAim} from "../../components/MesteqsAim";

const Footer = () => {
    return (
        <footer>
            <Link
                to="home"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                className="footer-top"
            >
                <ArrowUpwardIcon color="action" className="footer-arrow-icon"/>
                Top
            </Link>

            <Grid container>
                <div className="row">
                    <Grid item md className="logo-aim-container">
                        <img src={logo} alt="logo" width="50" height="auto"/>
                        <span className="mesteqs-aim">{mesteqsAim}</span>
                        <img
                            src={threadNeedle}
                            width="30"
                            height="20"
                            alt="threadNeedle"
                            className="responsive thread-needle"
                        />
                    </Grid>
                    <Grid item xs className="contact-details">
                        <a
                            href="https://en.wikipedia.org/wiki/Charentsavan"
                            target="_blank"
                            rel="noopener noreferrer"
                            style={{
                                color: "#898994",
                                textDecoration: "underline",
                            }}
                        >
                            ЧАРЕНЦАВАН
                        </a>
                        <a
                            href="https://en.wikipedia.org/wiki/Kotayk_Province"
                            target="_blank"
                            rel="noopener noreferrer"
                            style={{
                                color: "#898994",
                                textDecoration: "none",
                            }}
                        >
                            КОТАЙКСКАЯ ПРОВИНЦИЯ
                        </a>
                        <a
                            href="https://en.wikipedia.org/wiki/Armenia"
                            target="_blank"
                            rel="noopener noreferrer"
                            style={{
                                color: "#898994",
                                textDecoration: "none",
                            }}
                        >
                            РЕСПУБЛИКА АРМЕНИЯ
                        </a>
                    </Grid>
                    <Grid item xs className="contact-details">
                        <a
                            href="mailto:mesteqs.company@gmail.com"
                            style={{
                                color: "#898994",
                                textDecoration: "none",
                            }}
                        >
                            <EmailIcon
                                color="action"
                                style={{
                                    verticalAlign: "middle",
                                    margin: "2px",
                                }}
                            />
                            <span> mesteqs.company@gmail.com</span>
                        </a>
                        <span>
              <CallIcon
                  color="action"
                  style={{
                      verticalAlign: "middle",
                      margin: "5px 6px 5px 2px",
                  }}
              />
              <a
                  href="tel:+37493271797"
                  style={{
                      color: "#898994",
                      textDecoration: "none",
                      marginRight: "10px",
                  }}
              >
                093-27-17-97
              </a>
              <a
                  href="tel:+37477992967"
                  style={{
                      color: "#898994",
                      textDecoration: "none",
                  }}
              >
                077-99-29-67
              </a>
              <br/>
              <br/>
              <div style={{marginBottom: "20px"}}>Подписывайтесь На Нас</div>
              <span className="fb-content">
                <a
                    href="https://www.facebook.com/pg/MES-TEQS-319968875385269/about/?ref=page_internal"
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{
                        textDecoration: "none",
                        fontWeight: "500",
                    }}
                >
                  FB
                </a>
              </span>
            </span>
                    </Grid>
                </div>
            </Grid>
            <div className="copyright-text">
                Copyrights MESTEQS {new Date().getFullYear()}
            </div>
        </footer>
    );
};

export default Footer;
