import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import CssBaseline from "@material-ui/core/CssBaseline";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import CategoryIcon from "@material-ui/icons/Category";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import FetchData from "../Data";

const FashionProducts = () => {
    const [state, setState] = React.useState(false);

    const handleDrawerToggle = () => {
        setState(!state);
    };
    const drawer = (
        <div>
            <div className="toolbar"/>
            <Divider/>
            <List>
                {["Category 1", "Category 2"].map((text) => (
                    <ListItem button key={text}>
                        <ListItemIcon>
                            <CategoryIcon/>
                        </ListItemIcon>
                        <ListItemText primary={text}/>
                    </ListItem>
                ))}
            </List>
            <Divider/>
        </div>
    );

    return (
        <div className="drawer-root">
            <CssBaseline/>
            <AppBar position="fixed" className="appBar">
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className="menuButton"
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h4" className="typography">
                        Товары
                    </Typography>
                </Toolbar>
            </AppBar>
            <nav className="drawer" aria-label="mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        variant="temporary"
                        open={state}
                        onClose={handleDrawerToggle}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer className="drawerPaper" variant="permanent" open>
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className="content">
                <div className="toolbar"/>
                <div>
                    <FetchData/>
                </div>
            </main>
        </div>
    );
};

export default FashionProducts;
