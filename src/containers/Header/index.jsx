import React, {Fragment, useEffect, useState} from "react";
import Navbar from "../../components/Navs";

const Header = () => {
    const [state, setState] = useState({className: ""});

    const foo = () => {
        window.scrollY > 20 && state.className === ""
            ? setState({...state, className: "scrolled"})
            : setState({...state, className: ""});
    }

    useEffect(() => {
        window.addEventListener("scroll", foo);

        return () => {
            window.removeEventListener("scroll", foo);
        };
    }, []);

    return (
        <Fragment>
            <header className={state.className}>
                <section className="header-content">
                    <Navbar/>
                </section>
            </header>
        </Fragment>
    );
};

export default Header;
