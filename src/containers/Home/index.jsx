import React from "react";
import Header from "../Header";
import Mesteqs from "../Mesteqs";
import Footer from "../Footer";

const Home = () => {
    return (
        <div id="home">
            <Header/>
            {<section className="container-content">
                <Mesteqs/>
            </section>}
            <Footer/>
        </div>
    )
}
export default Home;