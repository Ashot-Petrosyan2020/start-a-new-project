import React from "react";
import Slick from "../Slick";

const Mesteqs = () => {
    return (
        <div className="mesteqs-slick-main-root">
            <Slick/>
        </div>
    );
};

export default Mesteqs;
