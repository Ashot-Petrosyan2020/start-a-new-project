import React, {
    useState,
    Fragment,
    // useEffect
} from "react";
import Products from "./Products";
// import axios from "axios";
// import {productsUrl} from "../../components/URLs";
import {Typography} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import Pagination from "@material-ui/lab/Pagination";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import {productItems} from "../../components/ProductItemsPlaceHolder";

const Data = () => {
    const [pageSize, setPageSize] = useState(1);
    // const [images, setImages] = useState({products: []});
    const [currentPage, setCurrentPage] = useState(1);

    const pageSizes = [1, 8, 12, 16, 20];

    // useEffect(() => {
    //     const source = axios.CancelToken.source();
    //
    //     const getImages = () => {
    //         axios(productsUrl, {
    //             cancelToken: source.token,
    //         }).then((result) => {
    //             setImages({...images, products: result.data.products});
    //         });
    //     };
    //     getImages();
    //
    //     return () => {
    //         source.cancel();
    //     };
    // }, [pageSize]);

    // Get current posts
    const currentPosts = productItems.slice(
        pageSize * (currentPage - 1),
        pageSize * currentPage
    );

    console.log("productItems", productItems);

    // // take element id, do API call and render the product
    // const handleClick = async (event) => {
    //   const getId = event.id;
    //   // console.log("getId", getId);
    //   const result = await axios(`${baseURL}api/products/get?id=${getId}`);
    //   // console.log("result.data", result.data.images);
    //   setImages({ ...images, products: result.data.images });
    // };

    // Change page
    const handlePageChange = (event, pageNumber) => {
        setCurrentPage(pageNumber);
        console.log("currentPage", currentPage);
    };

    // Change page Size
    const handlePageSizeChange = (event, page) => {
        setPageSize(event.target.value);
        setCurrentPage(1);
    };

    return (
        <Fragment>
            <Products productItems={currentPosts}/>
            <div className="pageSize-control">
                <Typography className="pageSize-control-paragraph">
                    Products per page:
                </Typography>
                <FormControl className="pageSize-control-form">
                    <InputLabel id="demo-customized-select-label">Page Size</InputLabel>
                    <Select
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={pageSize}
                        onChange={handlePageSizeChange}
                    >
                        {pageSizes.map((size) => (
                            <MenuItem key={size} value={size}>
                                {size}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Pagination
                    color="primary"
                    page={currentPage}
                    className="pagination"
                    onChange={handlePageChange}
                    count={Math.ceil(productItems.length / pageSize)}
                    // onClick={() => handleClick({ id: 1 })}
                />
            </div>
        </Fragment>
    );
};
export default Data;
