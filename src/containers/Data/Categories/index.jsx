// import React, {Fragment, useEffect, useState} from "react";
// import {categoriesUrl} from "../../URLs/URLs";
// import axios from "axios";
//
// const FetchCategories = () => {
//     const [category, setCategory] = useState({categories: []});
//     useEffect(() => {
//         const getCategories = () => {
//             axios(categoriesUrl).then(result => {
//                 setCategory({...category, categories: result.data});
//             });
//         };
//         getCategories();
//     }, []);
//
//     return (
//         <Fragment>
//             {category.categories.map(category => (
//                 <div key={category.id}>{category.name}</div>
//             ))}
//         </Fragment>
//     )
// }
// export default FetchCategories;
