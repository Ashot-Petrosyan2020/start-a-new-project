import React, {useEffect, useState} from "react";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Box from '@material-ui/core/Box';
import {baseURL} from "../../../../components/URLs";
import {data} from "../../../../components/PlaceHolderData";

const productCardUrl = `${baseURL}api/products/get?id=1`;

const handleClickProductTitle = (event, data) => {
    return data.id;
}

const ProductTitle = () => {

    const [state, setState] = useState({product: []})

    useEffect(() => {

        const fetchProduct = async () => {
            const result = await axios(productCardUrl);
            setState({...state, product: result.data.images})
        };

        fetchProduct();

    }, []);


    console.log('image', state);

    return (
        <React.Fragment>
            <Card className="product-root"
                  onClick={(e) => handleClickProductTitle(e, data[0])}
            >
                <CardActionArea className="product-cardActionArea">
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="div">
                            <Box fontWeight="fontWeightBold" m={1}>
                                {data[0].title}
                            </Box>
                        </Typography>
                    </CardContent>
                    <CardMedia className="product-media" title="Contemplative Reptile">
                        <img src={`${baseURL}${state.product}`}
                             className="product-img"
                             alt="title"/>
                    </CardMedia>
                </CardActionArea>
                <Grid item xs={12} sm container className="product-gridContainer">
                    <Grid item xs container direction="column" spacing={2}>
                        <Grid item xs>
                            <Typography variant="body2" color="textSecondary">
                                ID: {data[0].id}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography
                                variant="body2"
                                style={{cursor: "pointer"}}
                                color="secondary"
                            >
                                Remove
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Typography variant="subtitle1">$19.00</Typography>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    );
}
export default ProductTitle;

