import React, {Fragment} from "react";
import {Route, Switch} from "react-router-dom";
import FashionProducts from "./containers/FashionProducts";
import Home from "./containers/Home";
// import ProductPythonJackets from "./containers/Data/ProductItems/PythonJackets";
// import ProductLawEnforceJackets from "./containers/Data/ProductItems/LawEnforceJackets";
import ProductTitle from "./containers/Data/ProductItems/Title";


function App() {
    return (
        <Fragment>
            <Switch>
                <Route path="/fashion" component={FashionProducts}/>
                <Route path="/productDetails/:id" component={ProductTitle}/>
                <Route path="/" component={Home}/>
            </Switch>
        </Fragment>
    );
}

export default App;
